load('costMap.mat');
reconstruct_Path(costs, lengthY, widthX, goalX, goalY, startX, startY, DT, DistanceTransformMatrix);

function reconstruct_Path(map, lengthY, widthX, goalX, goalY, startX, startY, DT,DistanceTransformMatrix)
    cost_path = map; %copy of cost information
    compare_value = Inf;
    nextX = goalX;
    nextY = goalY;
    xPath = [];
    yPath = [];
    xPath(1,:) = nextX;
    yPath(1,:) = nextY;
    position = 2;
    
    %find shortest path
    while nextX ~= startX || nextY ~= startY
    neigbours_path = getNeigbours(nextX,nextY,cost_path);
    cost_path(nextY,nextX) = Inf;
        for i = 1:length(neigbours_path)
            x = neigbours_path{i}(1);
            y = neigbours_path{i}(2);
            value = cost_path(y,x);
            if value < compare_value
                compare_value = value;
                nextX = x;
                nextY = y;
            end
        end
        %store x and y values of next node for plotting
        xPath(position,:) = nextX;
        yPath(position,:) = nextY;
        
        position = position +1;
        compare_value = Inf;
    end
    figure('name','Path finding');
    hold on;
    subplot(1,3,1);imagesc([1 widthX], [1 lengthY], DT);
    hold on;
    plot(xPath,yPath,'g-','MarkerIndices',1:1:lengthY),
    colorbar;
    subplot(1,3,2); imagesc([1 widthX], [1 lengthY], DistanceTransformMatrix);
    hold on;
    plot(xPath,yPath,'g-','MarkerIndices',1:1:lengthY);
    subplot(1,3,3);mesh(map);title('total costs');
end

%same function as in A_star to geht neigbours with minimal costs
function Values = getNeigbours(x,y,map)
    [dimY, dimX] = size(map);
    Values = [];
    k = 1;
    for i = (x -1):(x +1)
        for j = (y -1):(y +1)
            if i >0 && j >0 && (i <=dimX )&& (j<=dimY) && (map(j,i)< map(y,x))
                Values{k} = [i j];
                k= k+1;
            end
        end
    end
end
