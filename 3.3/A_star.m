DT = imread('/Users/Leoni/Desktop/CMU/planning/4_1_map.png','png');
distValue = 10;
% DT = [0 0 0 0 0 0 0 0 0 0;
%                      0 0 0 0 0 0 0 0 0 0;
%                      1 0 0 1 1 1 1 0 0 1;
%                      0 0 0 0 0 0 0 0 0 0;
%                      0 0 0 0 0 1 0 0 0 0;
%                      0 0 0 0 1 1 0 0 0 0;
%                      0 0 0 0 1 0 0 0 0 0;
%                      0 0 0 0 0 0 0 1 1 1;
%                      0 0 0 0 0 0 0 0 0 0;
%                      0 0 0 0 0 0 0 0 0 0];
% distValue = 0.5;
DistanceTransformMatrix = bwdist(DT);
% get bounds of matrix
[lengthY, widthX] = size(DistanceTransformMatrix); 
%closedList- holds list of all processed nodes
closedNodes = zeros(lengthY,widthX); 
%openList - holds cost values of possible neigbour nodes
openNodes = Inf(lengthY,widthX);   
%cost_so_far - holds values of current node's gCost
costs = Inf(lengthY,widthX); 
%came_from - order of viewed nodes
nodeOrder = zeros(lengthY,widthX);                              

%initaial start x value and y value
startX = 140;                                                   
startY = 200; 
%initial goal x value and y value
goalX = 725;                                                    
goalY = 1095;                                                   

% calculates heurisitic Costs to goal
hCosts = zeros(lengthY,widthX);
hCosts(goalY,goalX) = 1;                                        
heuristicCosts = bwdist(hCosts);


%Initial Node
openNodes(startY,startX) = heuristicCosts(startY,startX);
closedNodes(startY,startX) = 1;
nodeOrder(startY,startX) = 1;
costs(startY,startX) = 0;   % gCosts of startNode
number = 2;                 % next node's number, iterative

while min(min(openNodes)) ~= Inf 
    %Get next node from open list
    [minval,idx] = min(openNodes(:));
    [currentY,currentX] = ind2sub(size(openNodes),idx);
    
    %reset minimum value
    openNodes(currentY,currentX) = Inf; 
    
    %check for goal node
    if isgoalnode(currentX,currentY,goalX,goalY)
        %reconstructPath(costs,lengthY,widthX, goalX, goalY, startX, startY, DT);
        goalfound = true;
        break;
    end
    
    %get neigbours from currentNode and add to openList
    neigbours = getNeigbours(currentX,currentY,heuristicCosts);
    for k = 1:length(neigbours)
        x = neigbours{k}(1);
        y = neigbours{k}(2);
        % check if node's already processed
            if closedNodes(y,x) == 1
                continue;
            end
            
          % keep 10 pixel distance to obstacles
            if DistanceTransformMatrix(y,x) > distValue
       
                dist = sqrt((currentX- x)^2 + (currentY -y)^2);
                gcost = costs(currentY,currentX) + dist;
                
                % only take node if value is min
                if (costs(y,x) >= gcost) || (costs(y,x) == 0)
                    hcost = gcost + heuristicCosts(y,x);
                    openNodes(y,x) = hcost;
                    costs(y,x) = gcost;
                    closedNodes(y,x) = 1;
                    
                    %set continuously order of nodes
                    nodeOrder(y,x) = number;
                    number = number+1;
                end  
            end               
    end   
          
end
save('costMap.mat','costs','lengthY','widthX','goalX','goalY','startX','startY','DT'); 

%checks node for goal node
function isgoal = isgoalnode(xcoord,ycoord,xgoal,ygoal)
    if (xcoord == xgoal) && (ycoord == ygoal)
        isgoal = true;
    else
        isgoal = false;
    end
    
end

%get neigbours with minimal costs
function Values = getNeigbours(x,y,map)
    [dimY, dimX] = size(map);
    Values = [];
    k = 1;
    for i = (x -1):(x +1)
        for j = (y -1):(y +1)
            if i >0 && j >0 && (i <=dimX )&& (j<=dimY) && (map(j,i)< map(y,x))
                Values{k} = [i j];
                k= k+1;
            end
        end
    end
end

%reconstruct path and draw
function reconstructPath(map, lengthY, widthX, goalX, goalY, startX, startY, DT)
    cost_path = map; %copy for no delete of cost information
    path = zeros(lengthY, widthX);
    compare_value = Inf;
    nextX = goalX;
    nextY = goalY;
    path(nextY, nextX) = 1000;
    while nextX ~= startX && nextY ~= startY
    neigbours_path = getNeigbours(nextX,nextY,cost_path);
    cost_path(nextY,nextX) = Inf;
        for i = 1:length(neigbours_path)
            x = neigbours_path{i}(1);
            y = neigbours_path{i}(2);
            value = cost_path(y,x);
            if value < compare_value
                compare_value = value;
                nextX = x;
                nextY = y;
            end
        end
        path(nextY, nextX) = 1000;
        compare_value = Inf;
    end
    path(startY,startX) = 1000;
    imagesc(DT);
    colorbar
end
